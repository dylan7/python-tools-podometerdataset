# Code in python 2.x

## Needed libraries:
    -Pip:
        $ sudo apt-get install python-pip python-dev build-essential
        $ sudo pip install --upgrade pip
    -Numpy:
        $ sudo pip install numpy
    -Matplotlib:
        $ sudo pip install matplotlib
    -Scipy:
        $ sudo pip install scikit-learn
        $ sudo pip install scipy
    -Dateutil:
        $ sudo pip install python-dateutil

## Usage:

### Dataset Correction:
    Example:
    $ python main.py -d /home/user/dataset -o GTv2.csv
    or download executable version and:
    $ ./datasetCorrector -d /home/user/dataset -o GTv2.csv

##### Params:
    -d,--dataset:
        path of the complete target dataset
        DEFAULT: ./

    -o,--output:
        name of the corrected GT file
        DEFAULT: GT.csv [overwrite current GT]

    -s:
        launch in supervised mode
        EXAMPLE: $ python main.py -s -d <dataset_path>

    --threshold:
        percentage of size needed to consider peak as a step. Should between 0 and 1
        DEFAULT: 0.5
        EXAMPLE: $ python main.py -d <dataset_path> --threshold 0.7

    --step_time:
        minimum time in seconds between step and GT to consider there is a false positive
        DEFAULT: 0.2 
        EXAMPLE: $ python main.py -d <dataset_path> --step_time 0.2


### Low pass filter:
    Example:
    $ python low_pass_filter.py -i /home/user/dataset_folder_1/file.avss -f 7,15

##### Params:
    -i,--input:
        path of the target file
        DEFAULT: ./

    -p:
        plot results
        Example: $ python low_pass_filter.py -i file.avss -p

    -f,--freq:
        list of cut frecuencies for low pass filters
        Exaples: $ python low_pass_filter.py -i file.avss -f 3,7,15

### Generate AVSS:
    Usage:
    $ python generateAVSS <dataset_path> <jar_files_path>
    
### Step segmentation:
    Example:
    $ python segmentation.py -d <dataset_path>
    or download executable version and:
    $ ./stepSegmentation -d <dataset_path>

##### Params:
    -d,--dataset:
        path of the complete target dataset
        DEFAULT: ./
        
    -p:
        plot segmentation
        EXAMPLE: $ python segmentation.py -p -d <dataset_path>
    
    --step-space:
        maximum time in seconds between steps to consider them consecutive 
        DEFAULT: 1
        EXAMPLE: $ python segmentation.py -d <dataset_path> --step-space 0.8
