import dateutil
from utils import *

GT_COLUMNS=['global_time_peak','acc_peak','global_time_valley','acc_valley']

#Constants
order = 1
fs = 100.0      # sample rate, Hz
cutoff = 5      # desired cutoff frequency of the filter, Hz

COMPLETE_GT = 0
ONLY_NEW_PEAKS = 1
WRITING_MODE = COMPLETE_GT

#plot configuration
labels = ['Pie derecho', 'Pie izquierdo', 'gt']
colors = ['-b','-g','oy']

class DatasetCorrection:

    def __init__(self,pieDer,pieIzq,gt,write_path,supervised,mean_threshold,step_time):

        self.acc_der = map(float,pieDer['acc_module'])
        self.acc_izq = map(float,pieIzq['acc_module'])
        self.acc_gt = np.array(map(float,gt['acc_peak']))

        self.timeDer = [dateutil.parser.parse(d) for d in pieDer['global_time']]
        self.timeIzq = [dateutil.parser.parse(d) for d in pieIzq['global_time']]
        self.timeGt = [dateutil.parser.parse(d) for d in gt['global_time_peak']]

        self.gt = gt
        self.path = write_path
        self.supervised = supervised
        self.gt_index = 0
        self.mean_threshold = mean_threshold
        self.step_time = step_time


    def process_data(self):
        self.filtered_der = butter_lowpass_filter(self.acc_der, cutoff, fs, order)
        self.filtered_izq = butter_lowpass_filter(self.acc_izq,cutoff,fs,order)

        self.smooth_der = savgol_filter(self.filtered_der,11,4)
        self.smooth_izq = savgol_filter(self.filtered_izq,11,4)

        self.peaksDer,_ = peakdetect(self.smooth_der,lookahead = 17, delta = 1.1)
        self.peaksIzq,_ = peakdetect(self.smooth_izq,lookahead = 17, delta = 1.1)
        self.peaksDer = [i[0] for i in self.peaksDer]
        self.peaksIzq = [i[0] for i in self.peaksIzq]
        try:
            self.der_threshold = self.mean_threshold*np.mean([self.smooth_der[i] for i in self.peaksDer])
            self.izq_threshold = self.mean_threshold*np.mean([self.smooth_izq[j] for j in self.peaksIzq])
        except:
            self.der_threshold = 0
            self.izq_threshold = 0

    def find_false_negatives(self):

        if WRITING_MODE == COMPLETE_GT:
            self.filewriter = Filewriter(self.path,GT_COLUMNS)
        elif WRITING_MODE == ONLY_NEW_PEAKS:
            self.filewriter = Filewriter(self.path,['global_time_peak','acc_peak'])

        #Plot configuration
        if self.supervised:
            signals = [self.smooth_der,self.smooth_izq, self.acc_gt]
            times = [self.timeDer,self.timeIzq,self.timeGt]
            peaks = [self.peaksDer,self.peaksIzq]

            self.plotConfiguration = PlotConfiguration(signals,times,labels,colors,peaks,self)

        count = 0

        #Find false negatives
        for pd in self.peaksDer:
            self.current_der_peak = pd
            index_derpeak = self.timeDer[pd]
            for pi in self.peaksIzq:
                self.current_izq_peak = pi
                index_izqpeak = self.timeIzq[pi]
                diff_time = abs(index_derpeak-index_izqpeak)
                if (diff_time.total_seconds() < 0.2) and (self.smooth_der[pd]>self.der_threshold) and (self.smooth_izq[pi]>self.izq_threshold):
                    if not any([(abs(gtTime-index_derpeak).total_seconds() < self.step_time) or (abs(gtTime-index_izqpeak).total_seconds() < self.step_time) for gtTime in self.timeGt]):
                        self.meanTime = meanDate(index_derpeak,index_izqpeak)
                        if not self.supervised:
                            self.updateGt()
                        else:
                            print 'Press [y]/[ENTER] to add the false negative to dataset or [n]/[ESCAPE] to discard it'
                            self.plotConfiguration.setPeak(self.meanTime)

                        count+=1

        if self.supervised:
            self.plotConfiguration.close()
        if WRITING_MODE == COMPLETE_GT:
            for g in range(self.gt_index,len(self.gt['global_time_peak'])):
                self.filewriter.writeRow([self.gt['global_time_peak'][g],self.gt['acc_peak'][g],self.gt['global_time_valley'][g],self.gt['acc_valley'][g]])
        self.filewriter.close()

        return count,len(self.acc_gt)

    def updateGt(self):
        minDer = self.current_der_peak-20 if self.current_der_peak-20 >= 0 else 0
        maxDer = self.current_der_peak+20 if self.current_der_peak+20 < len(self.acc_der) else len(self.acc_der)-1
        minIzq = self.current_izq_peak-20 if self.current_izq_peak-20 >= 0 else 0
        maxIzq = self.current_izq_peak+20 if self.current_izq_peak+20 < len(self.acc_izq) else len(self.acc_izq)-1

        peak_acc = max([max(self.acc_der[minDer:maxDer]),max(self.acc_izq[minIzq:maxIzq])])
        peak_time = self.meanTime
        time_str = peak_time.strftime('%H:%M:%S.%f')[:-3]
        print 'Writing new peak in GT'
        if WRITING_MODE == COMPLETE_GT:
            valley2_acc = min([min(self.acc_der[self.current_der_peak:maxDer]),min(self.acc_izq[self.current_izq_peak:maxIzq])])
            try:
                valley2_time = self.timeDer[self.acc_der[self.current_der_peak:maxDer].index(valley2_acc)+self.current_der_peak]
            except:
                valley2_time = self.timeIzq[self.acc_izq[self.current_izq_peak:maxIzq].index(valley2_acc)+self.current_izq_peak]

            for g in range(self.gt_index,len(self.gt['global_time_peak'])):
                if (g+1)==len(self.gt['global_time_peak']):
                    previous_time = dateutil.parser.parse(self.gt['global_time_valley'][g])
                    if  previous_time > peak_time:
                        valley1_acc = min([min(self.acc_der[minDer:self.current_der_peak]),min(self.acc_izq[minIzq:self.current_izq_peak])])
                        try:
                            valley1_time = self.timeDer[self.acc_der[minDer:self.current_der_peak].index(valley1_acc)+minDer]
                        except:
                            valley1_time = self.timeIzq[self.acc_izq[minIzq:self.current_izq_peak].index(valley1_acc)+minIzq]
                    else:
                        valley1_time = previous_time
                        valley1_acc = self.gt['acc_valley'][g]
                    self.filewriter.writeRow([self.gt['global_time_peak'][g],self.gt['acc_peak'][g],valley1_time.strftime('%H:%M:%S.%f')[:-3],str(valley1_acc)])
                    self.filewriter.writeRow([time_str,str(peak_acc),valley2_time.strftime('%H:%M:%S.%f')[:-3],str(valley2_acc),'new'])
                    break
                next_time = dateutil.parser.parse(self.gt['global_time_peak'][g+1])
                current_time = dateutil.parser.parse(self.gt['global_time_peak'][g])
                if current_time > peak_time:
                    self.gt_index = g
                    self.filewriter.writeRow([time_str,str(peak_acc),valley2_time.strftime('%H:%M:%S.%f')[:-3],str(valley2_acc),'new'])
                    break
                elif next_time > peak_time :
                    previous_time = dateutil.parser.parse(self.gt['global_time_valley'][g])
                    if  previous_time > peak_time:
                        valley1_acc = min([min(self.acc_der[minDer:self.current_der_peak]),min(self.acc_izq[minIzq:self.current_izq_peak])])
                        try:
                            valley1_time = self.timeDer[self.acc_der[minDer:self.current_der_peak].index(valley1_acc)+minDer]
                        except:
                            valley1_time = self.timeIzq[self.acc_izq[minIzq:self.current_izq_peak].index(valley1_acc)+minIzq]
                    else:
                        valley1_time = previous_time
                        valley1_acc = self.gt['acc_valley'][g]
                    self.filewriter.writeRow([self.gt['global_time_peak'][g],self.gt['acc_peak'][g],valley1_time.strftime('%H:%M:%S.%f')[:-3],str(valley1_acc)])
                else:
                    self.filewriter.writeRow([self.gt['global_time_peak'][g],self.gt['acc_peak'][g],self.gt['global_time_valley'][g],self.gt['acc_valley'][g]])

        elif WRITING_MODE == ONLY_NEW_PEAKS:
            self.filewriter.writeRow([time_str,str(peak_acc)])
