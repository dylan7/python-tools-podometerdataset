from utils import *
import dateutil
import sys
import getopt

def low_pass_filter(argv):
    # FILE PARAMS
    DATA_PATH = '/home/dylan/Documents/para Fernando/21-02-18_10_28_36@main_AVSS.csv'
    COLUMNS = ['global_time','a_vertical']
    PLOT_RESULTS = False
    SAVE_RESULTS = True

    # FILTER PARAMS
    order = 1
    fs = 100.0          # sample rate, Hz
    cutoff = [3,7,15]     # desired cutoff frequency of the filter, Hz

    try:
        opts, args = getopt.getopt(argv,"pi:f:",["input=","freq="])
    except getopt.GetoptError:
        print 'error reading arguments'
        sys.exit(0)
    for opt, arg in opts:
        if opt == '-p':
            PLOT_RESULTS = True
        elif opt in ("-i", "--input"):
            DATA_PATH = arg
        elif opt in ("-f","--freq"):
            cutoff = arg.split(',')
            cutoff = [float(c) for c in cutoff]

    ##################################################################################
    ##################################################################################

    # DATA READ
    data = read_csv(DATA_PATH)

    # DATA PROCESSING
    times = [dateutil.parser.parse(d) for d in data[COLUMNS[0]]]
    acc = map(float,data[COLUMNS[1]])

    filtered_signal = []
    for c in cutoff:
        filtered_signal.append(butter_lowpass_filter(acc,c,fs,order))

    # acc_smooth = savgol_filter(acc_low_freq,11,4)

    # PLOT
    if PLOT_RESULTS:
        signals = [acc]+filtered_signal
        times = [times]*(len(filtered_signal)+1)
        labels = ['RAW']+['cut '+str(x)+'Hz' for x in cutoff]
        colors = ['-r','-g','-b','-y','-c','-m','-k']
        plotConf = PlotConfiguration(signals,times,labels,colors)
        plt.show()


    # WRITE RESULTS IN CSV
    if SAVE_RESULTS:
        prefix = DATA_PATH.split('@')[0]
        for c in cutoff:
            path = prefix+'@main_filtered_'+str(c)+'Hz.csv'
            filewriter = Filewriter(path,COLUMNS)
            for i in range(len(data[COLUMNS[0]])):
                filewriter.writeRow([data[COLUMNS[0]][i], str(filtered_signal[cutoff.index(c)][i])])
            filewriter.close()

    print 'FINISH'


if __name__ == '__main__':
    low_pass_filter(sys.argv[1:])
