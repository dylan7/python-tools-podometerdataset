from __future__ import division
import numpy as np
from scipy.stats import *
import math
import matplotlib.pyplot as plt

PLOT = False

class gaussian_gen(rv_continuous):
    def _pdf(self, x):
        return np.exp(-x**2 / 2.) / np.sqrt(2.0 * np.pi)

def getPercentiles(frame,percentiles = [25,50,75]):
    result = []
    for p in percentiles:
        result.append(np.percentile(frame,p))
    return result

def getKurtosis(frame):
    frame = np.array(frame)
    return [kurtosis(frame,axis=None)]

#PETA SI HAY SAMPLE 0
def getHMean(frame):
    frame = np.array(frame)
    return [hmean(frame,axis=None)]

def getSkewness(frame):
    frame = np.array(frame)
    return [skew(frame,axis=None)]

def getVariation(frame):
    frame = np.array(frame)
    return [variation(frame,axis=None)]

#TARDA INFINITO ALGUNO
def getMLE(frame):
    gaussian = gaussian_gen(name='gaussian')
    result = gaussian.fit(np.array(frame))
    return list(result)

def getPolyCoefficients(frame,order=4):
    z = np.polyfit(range(len(frame)),frame,order)
    if PLOT:
        p = np.poly1d(z)
        xp = np.linspace(0, len(frame), 100)
        plt.plot(range(len(frame)),frame,'g-',xp,p(xp),'r-')
        plt.show()
    return list(z)

feature_extraction_names = ['Percentile25','Percentile50','Percentile75','Kurtosis','HMean','Skewness','Variation','MLE','PolyFitCoef0','PolyFitCoef1','PolyFitCoef2','PolyFitCoef3','PolyFitCoef4']
features_extraction_list = [getPercentiles,getKurtosis,getHMean,getSkewness,getVariation,getMLE,getPolyCoefficients]

def getAllFeatures(frame):
    features = []
    for f in features_extraction_list:
        features = features + f(frame)
    return features
