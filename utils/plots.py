import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as md
from filereader import *
from filewriter import *
import time

def meanDate(date1,date2):
    diff_time = abs(date1-date2)
    meanTime = date1
    if date1 > date2:
        meanTime = date2 + (diff_time/2)
    else:
        meanTime = date1 + (diff_time/2)
    return meanTime

class PlotConfiguration:
    def __init__(self, signals, times, labels,colors, peaks= [],key_handler = None):

        self.signals = signals
        self.times = times
        self.peaks = peaks
        self.labels = labels
        self.colors = colors

        self.ax = plt.gca()
        self.fig = plt.gcf()
        self.key_handler = key_handler

        plt.subplots_adjust(bottom=0.2)
        plt.xticks( rotation= 30 )
        xfmt = md.DateFormatter('%H:%M:%S.%f')
        self.ax.xaxis.set_major_formatter(xfmt)
        if key_handler != None:
            self.fig.canvas.mpl_connect('key_press_event', self)

        for p in range(len(peaks)):
            for index in peaks[p]:
                plt.plot(times[p][index],signals[p][index],'or')

        for s in range(len(signals)):
            plt.plot(times[s],signals[s],colors[s],label = labels[s])

        plt.legend(loc='best')
        #plt.show()

    def __call__(self,event):
        if event.key == 'y' or event.key=='enter':
            self.key_handler.updateGt()

        elif event.key == 'n' or event.key=='escape':
            print 'Nothing did'

    def show(self):
        if not plt.get_fignums():
            plt.show(block=False)

    def forceShow(self):
        try:
            plt.show()
        except:
            print('cant show')

    def setPeak(self,meanTime):
        self.ax.axvline(meanTime, linestyle='--', color = 'r', alpha = 0.5)
        self.ax.set_xlim([meanTime-datetime.timedelta(0,1),meanTime+datetime.timedelta(0,1)])
        self.fig.canvas.draw()
        self.fig.waitforbuttonpress()

    def getAxes(self):
        return self.ax

    def close(self):
        plt.close()
        self.fig.clear()
