import csv
import os

#functions
def read_csv(path):
    with open(path,'rb') as csvfile:
        spamreader = csv.reader(csvfile,delimiter=',',quotechar='|')
        list_data = []
        for row in spamreader:
            row_data = []
            for column in row:
                row_data.append(column.strip(' '))
            list_data.append(row_data)
        column_names = list_data[0]
        list_data = list_data[1::]
        data = dict()
        for column in range(len(column_names)):
            data[column_names[column]] = [item[column] for item in list_data]
    return data

def findfiles(files,filenames):
    found = [False] * len(filenames)
    paths = [None] * len(filenames)
    for f in files:
        for i in range(len(filenames)):
            if not found[i]:
                if f.endswith(filenames[i]):
                    found[i]=True
                    paths[i]=f
    return all(found),paths
