import csv
import os

class Filewriter:

    def __init__(self,path,columns):
        if os.path.isfile(path):
             os.remove(path)
        self.file = open(path, 'wb')
        self.writer = csv.writer(self.file, delimiter =',',quoting=csv.QUOTE_MINIMAL)
        self.writer.writerow(columns)

    def writeRow(self,data):
        self.writer.writerow(data)

    def close(self):
        self.file.close()
