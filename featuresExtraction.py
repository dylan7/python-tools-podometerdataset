from utils import *
import os,sys,getopt
import dateutil

DATA_PATH = '/home/dylan/ownCloud/InnovaPeme-FUAC/Dataset_Podometro/DATA/'
DATA_PATH = '/home/dylan/ownCloud/InnovaPeme-FUAC/Dataset_Podometro/DATA/Carlos V Regueiro/15-12-17_10:51:26/'

for root,subFolders,files in os.walk(DATA_PATH):
    found,paths = findfiles(files,['@steps.csv'])
    if found:
        print 'Processing folder '+root
        steps = read_csv(os.path.join(root,paths[0]))

        times = steps['global_time']
        acc = steps['a_vertical']

        steps=[]
        auxt=[]
        auxa=[]

        for index in range(len(times)):
            if times[index]=='*':
                stp=[[dateutil.parser.parse(d) for d in auxt]]
                stp.append(map(float,auxa))
                steps.append(stp)
                auxt=[]
                auxa=[]
            else:
                auxt.append(times[index])
                auxa.append(acc[index])

        for s in steps:
            a = getAllFeatures(s[1])
            plot = PlotConfiguration([s[1]],[s[0]],['main'],['-b'])
            plot.forceShow()
