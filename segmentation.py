from utils import *
import dateutil
from datetime import timedelta
import os,sys,getopt

def findStepBorders(step,mins,min_width,max_width,time_main):
    leftCutMin = step-min_width
    leftCutMax = step-max_width
    leftCut = leftCutMax
    for m in mins:
        if (m>leftCutMax) and (m<leftCutMin):
            leftCut = m

    rightCutMin = step+min_width
    rightCutMax = step+max_width
    rightCut = rightCutMax
    for m in reversed(mins):
        if (m<rightCutMax) and (m>rightCutMin):
            rightCut = m

    return findDate(time_main,leftCut),findDate(time_main,rightCut)

def findDate(list,date):
    for l in range(len(list)):
        if l+1 == len(list):
            return list[l]
        if list[l+1]>date :
            return list[l] if abs(list[l]-date)<abs(list[l+1]-date) else list[l+1]


def findStepBorder(mins,step1,step2,margin,time_main):
    meanT = meanDate(step1,step2)
    leftMargin = meanT-margin
    rightMargin = meanT+margin
    segmentation = mins[0]

    for m in mins:
        if m>leftMargin and m<rightMargin:
            if abs(m-meanT) < abs(segmentation-meanT):
                segmentation = m
    if segmentation == mins[0]:
        segmentation = findDate(time_main,meanT)

    return segmentation

def main(argv):

    DATA_PATH = './'
    # DATA_PATH = '/home/dylan/ownCloud/InnovaPeme-FUAC/Dataset_Podometro/DATA/Carlos V Regueiro/15-12-17_10:51:26/'
    MIN_WIDTH = timedelta(seconds = 0.08)
    MAX_WIDTH = timedelta(seconds = 0.27)
    MEAN_MARGIN = timedelta(seconds = 0.2)
    MAX_STEP_SPACE = timedelta(seconds = 1)
    PLOT = False

    try:
        opts, args = getopt.getopt(argv,"pd:",["dataset=","step-space"])
    except getopt.GetoptError:
        print 'error reading arguments'
        sys.exit(0)
    for opt, arg in opts:
        if opt == '-p':
            PLOT = True
        elif opt in ("-d", "--dataset"):
            DATA_PATH = arg
        elif opt in ("--step-space"):
            MAX_STEP_SPACE = float(arg)

    for root,subFolders,files in os.walk(DATA_PATH):
        found,paths = findfiles(files,['@main_AVSS.csv','@GT.csv'])
        if found:
            print 'Processing folder '+root
            main = read_csv(os.path.join(root,paths[0]))
            gt = read_csv(os.path.join(root,paths[1]))

            acc_main = map(float,main['a_vertical'])

            _,min_peaks = peakdetect(acc_main,lookahead=5,delta=0.3)
            min_peaks = [i[0] for i in min_peaks]

            gt = [dateutil.parser.parse(d) for d in gt['global_time_peak']]
            time_main = [dateutil.parser.parse(d) for d in main['global_time']]
            if not time_main:
                print 'error: No times'
                continue
            min_peaks_times = [time_main[j] for j in min_peaks]

            writer = Filewriter(os.path.join(root,paths[0].split('@')[0]+'@steps.csv'),['global_time','a_vertical'])
            writerSegm = Filewriter(os.path.join(root,paths[0].split('@')[0]+'@segmentation.csv'),['global_time','state'])
            states = [0] * len(time_main)

            if PLOT:
                plot = PlotConfiguration([acc_main],[time_main],['main'],['-b'],[min_peaks])
                ax = plot.getAxes()
                for m in gt:
                    ax.axvline(m,linestyle='-',color='g',alpha=0.5)

            # for p in gt:
            #     leftCut,rightCut = findStepBorders(p,min_peaks_times,MIN_WIDTH,MAX_WIDTH)
            #     if PLOT:
            #         ax.axvline(leftCut,linestyle='--',color='b',alpha=0.3)
            #         ax.axvline(rightCut,linestyle='--',color='b',alpha=0.3)
            #     leftIndex,rightIndex = findDate(time_main,leftCut),findDate(time_main,rightCut)
            #
            #     for index in range(leftIndex,rightIndex):
            #         t,a = time_main[index],acc_main[index]
            #         writer.writeRow([t.strftime('%H:%M:%S.%f')[:-3],str(a)])
            #     writer.writeRow(['*','*'])

            for i in range(len(gt)):
                if i == 0:
                    leftCut,_ = findStepBorders(gt[i],min_peaks_times,MIN_WIDTH,MAX_WIDTH,time_main)
                elif abs(gt[i]-gt[i-1])>MAX_STEP_SPACE:
                    leftCut,_ = findStepBorders(gt[i],min_peaks_times,MIN_WIDTH,MAX_WIDTH,time_main)
                else:
                    leftCut = rightCut

                if i+1 == len(gt):
                    _,rightCut = findStepBorders(gt[i],min_peaks_times,MIN_WIDTH,MAX_WIDTH,time_main)

                else:
                    if abs(gt[i]-gt[i+1]) > MAX_STEP_SPACE:
                        _,rightCut = findStepBorders(gt[i],min_peaks_times,MIN_WIDTH,MAX_WIDTH,time_main)
                    else:
                        rightCut = findStepBorder(min_peaks_times,gt[i],gt[i+1],MEAN_MARGIN,time_main)

                if PLOT:
                    ax.axvline(leftCut,linestyle='--',color='b',alpha=0.3)
                    ax.axvline(rightCut,linestyle='--',color='b',alpha=0.3)

                leftIndex,rightIndex = time_main.index(leftCut),time_main.index(rightCut)
                for index in range(leftIndex,rightIndex):
                     t,a = time_main[index],acc_main[index]
                     writer.writeRow([t.strftime('%H:%M:%S.%f')[:-3],str(a)])
                     states[index]=1
                writer.writeRow(['*','*'])

            for j in range(len(time_main)):
                writerSegm.writeRow([time_main[j].strftime('%H:%M:%S.%f')[:-3],str(states[j])])

            writer.close()
            writerSegm.close()
            if PLOT:
                plot.forceShow()

if __name__ == '__main__':
    main(sys.argv[1:])
