import sys
import subprocess
from utils import *

try:
    DATA_PATH = sys.argv[1]
    JAR_PATH = sys.argv[2]
except:
    DATA_PATH = '/home/dylan/ownCloud/InnovaPeme-FUAC/Dataset_Podometro/DATA/'
    JAR_PATH = '/home/dylan/ownCloud/Documents/'

for root,subFolders,files in os.walk(DATA_PATH):
    found, paths = findfiles(files,['@main_AV.csv'])
    if found:
        os.remove(os.path.join(root,paths[0]))
    found,paths = findfiles(files,['@main_RAW.csv'])
    if found:
        print 'Processing: '+root
        FNULL = open(os.devnull, 'w')
        subprocess.call(['java', '-jar', JAR_PATH+'0generadorCuaterniones.jar',os.path.join(root,paths[0])],stdout=FNULL, stderr=subprocess.STDOUT)

for root,subFolders,files in os.walk(DATA_PATH):
    found, paths = findfiles(files,['@main_AVSS.csv'])
    if found:
        os.remove(os.path.join(root,paths[0]))
    found,paths = findfiles(files,['@main_AV.csv'])
    if found:
        print 'Processing: '+root
        FNULL = open(os.devnull, 'w')
        subprocess.call(['java', '-jar', '/home/dylan/ownCloud/Documents/1subsampler.jar',os.path.join(root,paths[0])],stdout=FNULL, stderr=subprocess.STDOUT)
