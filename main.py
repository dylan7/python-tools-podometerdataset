import sys
import subprocess
from utils import *
import getopt

def main(argv):

    DATA_PATH = './'
    #DATA_PATH = '/home/dylan/Downloads/dataset/DATA (GTv3)/Otros voluntarios/Situm/Usuario5/mano/21-12-17_09:54:38'
    OUTPUT = 'GT.csv'
    SUPERVISED = False
    mean_threshold = 0.50
    step_time = 0.2

    #Take arguments
    try:
        opts, args = getopt.getopt(argv,"sd:o:",["dataset=","output=","threshold=","step_time="])
    except getopt.GetoptError:
        print 'error reading arguments'
        sys.exit(0)
    for opt, arg in opts:
        if opt == '-s':
            SUPERVISED = True
        elif opt in ("-d", "--dataset"):
            DATA_PATH = arg
        elif opt in ("-o", "--output"):
            OUTPUT = arg
        elif opt in ("--threshold"):
            mean_threshold = float(arg)
        elif opt in ("--step_time"):
            step_time = float(arg)

    total_count = 0
    experiments = 0

    #Folder iteration
    for root,subFolders,files in os.walk(DATA_PATH):
        found,paths = findfiles(files,['@pieDcho_ACC.csv','@pieIzqdo_ACC.csv','@GT.csv'])

        if found: #IF ALL THE NEEDED FILES ARE FOUND, WE CAN READ THEM
            print 'PROCESSING FOLDER: '+root
            prefix = os.path.join(root,paths[0].split('@')[0])
            pieDer = read_csv(os.path.join(root,paths[0]))
            pieIzq = read_csv(os.path.join(root,paths[1]))
            gt = read_csv(os.path.join(root,paths[2]))
            datasetCorrector = DatasetCorrection(pieDer,pieIzq,gt,prefix+'@'+OUTPUT,SUPERVISED,mean_threshold,step_time)
            datasetCorrector.process_data()
            count,len_gt = datasetCorrector.find_false_negatives()
            print str(count)+' new steps added'
            total_count+=count
            experiments+=1
            total = len_gt+count
            txtfound, txtpath = findfiles(files,['@NOTES.txt'])
            if txtfound:
                f = open(os.path.join(root,txtpath[0]),'a')
                f.write('\t- corrected GT:\n')
                f.write('\t\t\t\t\t'+str(total)+' steps\n')

    print 'Total steps added: '+str(total_count)+' in '+str(experiments)+' folders'

if __name__ == '__main__':
    main(sys.argv[1:])
